FROM golang:latest

RUN mkdir /test
WORKDIR /test
ADD . /test

RUN go mod download

ENTRYPOINT CompileDaemon --build="go build main.go" --command=./main